<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

INCLUDE "classes/config.php";
INCLUDE "classes/DBConnection.php";
	
$Vmije32iljj3 = new DBConnection();
$Vv2oj1ejnszs = $Vmije32iljj3->connection;

if(logValidate() == true){  

if($_SESSION['role'] == "Admin" || $_SESSION['level'] == 1){
?>
<html>
<head>
    <link rel="stylesheet" href="css/jquery.dataTables.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/jquery.dataTables.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/app.js"></script>
    <script src = "js/jquery.jclock.js"></script>
</head>
<body>
<div class = "wrapper">
<?php
INCLUDE "partials/addDVMdialog.php";
INCLUDE "partials/editDVMdialog.php";
?>
    <div class = "user">
        <div class = "back">
            <a href = "index.php" class = "log-back">
                <div class = "mn-btn">
                    <span class="mif-arrow-left mif-4x bg-transparent fg-darkGreen"></span>
                </div>
                
            </a>
            <div class = "mn-title">
                <span class = "title">BACK</span>
            </div>
        </div>
        <h1 class = "margin30">DVM SETTINGS</h1>
    </div>
        
    <hr class="thin"/>
    <br/>

    <div class = "margin20">
        <button class="button primary" name = "submit" onClick = "metroDialog.open('#addDVMdialog')">ADD DVM</button>
        <button id = "editDVM"class="button primary">UPDATE DVM</button>
        <button id = "deleteDVM" class="button warning">DELETE DVM</button>
    </div>

                <div id = "table">
                    <table id="dvms" class="border bordered hovered">
                        <thead>
                            <tr>
                                <th>DVM ID</th>
                                <th>DVM NANE</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php

                                $Vj5cx1u0aa4r = $Vv2oj1ejnszs->query("SELECT * FROM dvm");

                                foreach($Vj5cx1u0aa4r as $Vi0pa1ur3xsb){
                                    echo "<tr>
                                        <td>".$Vi0pa1ur3xsb['id']."</td>
                                        <td>".$Vi0pa1ur3xsb['name']."</td>
                                    </tr>";
                                    }

                            ?>
                        </tbody>
                    </table>
                </div>

</div>
</body>
</html>

<?php
    }
    else{
        $Vhtqnw2fdigh = "Restricted zone";
        setNotice($Vhtqnw2fdigh);
        header("location:index.php");
    }
}
else{
    $Vhtqnw2fdigh = "Restricted zone";
    setNotice($Vhtqnw2fdigh);
    header("location:login.php");
}
?>