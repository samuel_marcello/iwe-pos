<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

INCLUDE "classes/config.php";
INCLUDE "classes/DBConnection.php";

$Vmije32iljj3 = new DBConnection();
$Vv2oj1ejnszs = $Vmije32iljj3->connection;

if(logValidate() == true){  

if($_SESSION['role'] == "Admin" || $_SESSION['role'] == "Accountant"){
?>
<html>
<head>
    <link rel="stylesheet" href="css/jquery.dataTables.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/jquery.dataTables.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/app.js"></script>
    <script src = "js/jquery.jclock.js"></script>
</head>
<body>
    <div class = "wrapper">
<?php
INCLUDE "partials/addFarmerDialog.php";
INCLUDE "partials/editFarmerDialog.php";
?>
    <div class = "user">
        <div class = "back">
            <a href = "index.php" class = "log-back">
                <div class = "mn-btn">
                    <span class="mif-arrow-left mif-4x bg-transparent fg-darkGreen"></span>
                </div>

            </a>
            <div class = "mn-title">
                <span class = "title">BACK</span>
            </div>
        </div>
        <h1 class = "margin30">PAYMENTS</h1>
    </div>

    <hr class="thin"/>
    <br/>
                <div id = "table">
                    <table id="invoices" class="border bordered">
                        <thead>
                            <tr>
                                <th>INVOICE SERIAL</th>
                                <th>DVM</th>
                                <th>INVOICED DATE</th>
                                <th>PAYMENT</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php

                                $Vxmng3unon1m = $Vv2oj1ejnszs->query("SELECT i.*, DVM.name FROM invoice AS i
                                                        INNER JOIN dvm AS DVM
                                                        ON i.DVMID = DVM.id
                                                        WHERE paymentStatus = false
                                                        ");

                                foreach($Vxmng3unon1m as $V1gbcgttzxut){
                                    echo "<tr>
                                        <td>".$V1gbcgttzxut['invoiceSerialNo']."</td>
                                        <td>".$V1gbcgttzxut['name']."</td>
                                        <td>".$V1gbcgttzxut['invoiceDate']."</td>
                                        <td>";
                                        if($V1gbcgttzxut['paymentStatus'] == 0){echo "NOT PAID";}
                                        if($V1gbcgttzxut['paymentStatus'] == 1){echo "PAID";}
                                    echo "</td>";
                                        if($V1gbcgttzxut['paymentStatus'] == 0){
                                          echo '<td id = "pay'.$V1gbcgttzxut['invoiceID'].'"><button id = "pay" class="button primary"
                                                    onclick = "pay('.$V1gbcgttzxut['invoiceID'].')">PAY</button></td>';
                                        }else{echo "<td></td>";}

                                  echo "</tr>";
                                    }

                            ?>
                        </tbody>
                    </table>
                </div>

</div>
</body>
</html>

<?php
    }
    else{
        $Vhtqnw2fdigh = "Restricted zone";
        setNotice($Vhtqnw2fdigh);
        header("location:index.php");
    }
}
else{
    $Vhtqnw2fdigh = "Restricted zone";
    setNotice($Vhtqnw2fdigh);
    header("location:login.php");
}
?>
