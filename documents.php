<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

INCLUDE "classes/config.php";
INCLUDE "classes/DBConnection.php";

$Vmije32iljj3 = new DBConnection();
$Vv2oj1ejnszs = $Vmije32iljj3->connection;

if(logValidate() == true){  

if($_SESSION['role'] == "Admin" || $_SESSION['role'] == "Accountatnt"){
?>
<html>
<head>
    <link rel="stylesheet" href="css/jquery.dataTables.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/jquery.dataTables.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/app.js"></script>
    <script src = "js/jquery.jclock.js"></script>
</head>
<body>
    <div class = "wrapper">
<?php
INCLUDE "partials/addFarmerDialog.php";
INCLUDE "partials/editFarmerDialog.php";
?>
    <div class = "user">
        <div class = "back">
            <a href = "index.php" class = "log-back">
                <div class = "mn-btn">
                    <span class="mif-arrow-left mif-4x bg-transparent fg-darkGreen"></span>
                </div>

            </a>
            <div class = "mn-title">
                <span class = "title">BACK</span>
            </div>
        </div>
        <h1 class = "margin30">DOCUMENTS</h1>
    </div>

    <hr class="thin"/>
    <br/>
        <div class="flex-grid">
            <div class = "row">
                <div class = "cell colspan4">

                    <div class="input-control select">
                        <label>INVOICE PAYMENTS :</label>
                        <select id = "doc-payment">
                            <option valaue = "paid" selected="selected">PAID</option>
                            <option valaue = "not paid">NOT PAID</option>
                        </select>
                    </div>

                    <div class="input-control select">
                        <label>YEAR :</label>
                        <select id = "doc-year">
                            <option valaue = "2017" selected="selected">2017</option>
                            <option valaue = "2018">2018</option>
                            <option valaue = "2019">2019</option>
                        </select>
                    </div>



                    <ul id = "moy">
                        <li><button class="button lighten" onclick = "monthInvoices(1)">JANUARY</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(2)">FEBRUARY</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(3)">MARCH</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(4)">APRIL</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(5)">MAY</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(6)">JUNE</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(7)">JULY</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(8)">AUGUST</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(9)">SEPTEMBER</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(10)">OCTOBER</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(11)">NOVEMBER</button></li>
                        <li><button class="button lighten" onclick = "monthInvoices(12)">DECEMBER</button></li>
                    </ul>
                </div>
                <div class = "cell colspan8">
                    <div id = "documents">

                    </div>
                </div>
            </div>
        </div>

</div>
</body>
</html>

<?php
    }
    else{
        $V0etr115fjvw = "ERROR";
        $Vhtqnw2fdigh = "Access level not accepted";
        setNotice($V0etr115fjvw, $Vhtqnw2fdigh);
        header("location:index.php");
    }
}
else{
    $V0etr115fjvw = "ERROR";
    $Vhtqnw2fdigh = "You must loggin first";
	setNotice($V0etr115fjvw, $Vhtqnw2fdigh);
    header("location:login.php");
}
?>
