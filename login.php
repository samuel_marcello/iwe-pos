<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

?>

<html>
<head>
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/app.js"></script>

</head>
<body>
<div class = "wrapper">

    <div class = "comapny">
    </div>
    <div class = "banner">
    </div>

    <div class = "logger margin10">
        <form method = "post" action = "models/logModel.php">
            <h1 class="text-light">USER LOGIN</h1>
            <hr class="thin"/>
            <br />
            <div class="input-control text full-size" data-role="input">
                <label for="user_login">Usename:</label>
                <input type="text" name="username" id="username">
                <button class="button helper-button clear"><span class="mif-cross"></span></button>
            </div>
            <br />
            <br />
            <div class="input-control password full-size" data-role="input">
                <label for="user_password">User password:</label>
                <input type="password" name="password" id="password">
                <button class="button helper-button reveal"><span class="mif-looks"></span></button>
            </div>
            <br />
            <br />
            <div class="form-actions">
                <button type="submit" class="button primary" name = "submit" value = "Login">Login</button>
            </div>
        </form>
    </div>

</div>
</body>
</html>