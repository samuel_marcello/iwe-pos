<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

INCLUDE "classes/config.php";
INCLUDE "classes/DBConnection.php";

$Vmije32iljj3 = new DBConnection();
$Vv2oj1ejnszs = $Vmije32iljj3->connection;

if(logValidate() == true){  

if($_SESSION['role'] == "Admin" || $_SESSION['level'] == 1){
?>
<html>
<head>
    <link rel="stylesheet" href="css/jquery.dataTables.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/jquery.dataTables.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/app.js"></script>
    <script src = "js/jquery.jclock.js"></script>
</head>
<body>
    <div class = "wrapper">
<?php
INCLUDE "partials/addFarmerDialog.php";
INCLUDE "partials/editFarmerDialog.php";
?>
    <div class = "user">
        <div class = "back">
            <a href = "index.php" class = "log-back">
                <div class = "mn-btn">
                    <span class="mif-arrow-left mif-4x bg-transparent fg-darkGreen"></span>
                </div>

            </a>
            <div class = "mn-title">
                <span class = "title">BACK</span>
            </div>
        </div>
        <h1 class = "margin30">FARMERS</h1>
    </div>

    <hr class="thin"/>
    <br/>

    <div class = "margin20">
        <button class="button primary" name = "submit" onClick = "metroDialog.open('#addFarmerdialog')">ADD FARMER</button>
        <button id = "editFarmer"class="button primary">UPDATE FARMER</button>
        <button id = "deleteFarmer" class="button warning">DELETE FARMER</button>
    </div>

                <div id = "table">
                    <table id="farmers" class="border bordered hovered">
                        <thead>
                            <tr>
                                <th>FARMER ID</th>
                                <th>FIRST NANEM</th>
                                <th>LAST NAME</th>
                                <th>DISTRICT</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php

                                $Vnco4u4f1g0h = $Vv2oj1ejnszs->query("SELECT * FROM farmer");

                                foreach($Vnco4u4f1g0h as $Vounqbc3i2xg){
                                    echo "<tr>
                                        <td>".$Vounqbc3i2xg['farmerID']."</td>
                                        <td>".$Vounqbc3i2xg['firstName']."</td>
                                        <td>".$Vounqbc3i2xg['lastName']."</td>
                                        <td>".$Vounqbc3i2xg['district']."</td>
                                    </tr>";
                                    }

                            ?>
                        </tbody>
                    </table>
                </div>

</div>
</body>
</html>

<?php
    }
    else{
        $Vhtqnw2fdigh = "Restricted zone";
        setNotice($Vhtqnw2fdigh);
        header("location:index.php");
    }
}
else{
    $Vhtqnw2fdigh = "Restricted zone";
    setNotice($Vhtqnw2fdigh);
    header("location:login.php");
}
?>
