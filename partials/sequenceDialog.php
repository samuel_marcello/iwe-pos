        <!--START SEQUENCE DIALOG-->
<div data-role="dialog" id="OFSPdialog" class="padding20" data-close-button = "true" data-width = "500" data-height = "300"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>Select Input Mode</h1>
        <hr class="thin"/>
        <br/>

                <div class="input-control text full-size">
                    <label class="input-control radio">
                        <input type="radio" name = "sequence" value = "single" class = "seq-init">
                        <span class="check"></span>
                        <span id="timeOfDay"></span>
                        <span class="caption">Single sequence (Enter one voucher at a time)</span>
                    </label>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label class="input-control radio">
                        <input type="radio" name = "sequence" value = "batch" class = "seq-init">
                        <span class="check"></span>
                        <span class="caption">Batch sequence (Enter more than one voucher continously)</span>
                    </label>
                </div><br/><br/>  
             
            <div class="form-actions">
                <button type="submit" class="button primary" name = "submit" value = "sequence" onclick = "seqAction()">Ok</button>
            </div>
</div>
    <!--END DIALOG-->