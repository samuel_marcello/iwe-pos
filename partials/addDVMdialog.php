        <!--START ADD DVM DIALOG-->
<div data-role="dialog" id="addDVMdialog" class="padding20" data-close-button = "true" data-width = "500" data-height = "550"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>Enter DVM Details</h1>
        <hr class="thin"/>
        <br/>
        <form role = "form" method = "post" action = "models/DVMModel.php">
                <div class="input-control text full-size">
                    <label for = "DVMName">DVM Name</label>
                    <input type="text" name = "DVMName" id = "DVMName" REQUIRED>
                </div><br/><br/>

            <div class="form-actions center">
                <button type="submit" class="button primary" name = "submit" value = "Add">ENTER</button>
            </div>
        </form>
</div>
    <!--END DIALOG-->
