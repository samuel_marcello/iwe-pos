        <!--START EDIT FARMER DIALOG-->
<div data-role="dialog" id="editFarmerdialog" class="padding20" data-close-button = "true" data-width = "500" data-height = "550"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>Update Farmer Details</h1>
        <hr class="thin"/>
        <br/>
        <form role = "form" method = "post" action = "models/farmersModel.php">
          <input type="hidden" name = "farmerID" id = "farmerID" REQUIRED>
                <div class="input-control text full-size">
                    <label for = "firstName">First Name</label>
                    <input type="text" name = "firstName" id = "editfirstName" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "lastName">Last Name</label>
                    <input type="text" name = "lastName" id = "editlastName" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "district">District</label>
                    <input type="text" name = "district" id = "editdistrict" REQUIRED>
                </div><br/><br/>

            <div class="form-actions center">
                <button type="submit" class="button primary" name = "submit" value = "Update">ENTER</button>
            </div>
        </form>
</div>
    <!--END DIALOG-->
