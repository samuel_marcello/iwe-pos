<!--START USED VOUCHER DIALOG-->
<div data-role="dialog" id="usedVoucherdialog" class="padding20" data-close-button = "true" data-width = "550" data-height = "350"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>OFSP Voucher Used</h1>
        <hr class="thin"/>
        <br/>
        <input type = "hidden" id = "usedDVMID" value = "">
        <input type = "hidden" name = "vid" id = "usedv" value = "">
                <div class="input-control text" style = "width: 250px">
                    <label for = "serial">SERIAL</label>
                    <p id = "usedSerial"></p>
                </div>
                <div class = "usedBarcode"></div><br/><br/>

                <table class = "table bordered">
                    <theader>
                    <tr>
                        <th>DVM ID</th>
                        <th>DVM</th>
                        <th>DISTRICT</th>
                    </tr>
                    <theader>
                    <tbody>
                    <tr>
                        <td id = "DVMID"></td>
                        <td id = "usedDVM"></td>
                        <td id = "DVMdistrict"></td>
                    </tr>
                    </tbody>
                </table>

            <div class="form-actions">
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqExit()">EXIT</button>
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqUpdate()">UPDATE DETAILS</button>
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqStatus()">STATUS</button>
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqNew()">ENTER NEW</button>
            </div>
</div>
    <!--END DIALOG-->