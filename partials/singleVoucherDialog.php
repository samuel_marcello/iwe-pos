        <!--START SINGLE VOUCHER DIALOG-->
<div data-role="dialog" id="voucherdialog" class="padding20" data-close-button = "true" data-width = "500" data-height = "350"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>Enter OFSP Voucher</h1>
        <hr class="thin"/>
        <br/>

                <div class="input-control text" style = "width: 250px">
                    <label for = "serial">SERIAL</label>
                    <p id = "ro-serial"></p>
                </div>
                <div class = "barcode"></div><br/><br/>

                <table class = "table bordered">
                    <theader>
                    <tr>
                        <th>DVM</th>
                        <th>DISTRICT</th>
                    </tr>
                    <theader>
                    <tbody>
                    <tr>
                        <td id = "DVM"></td>
                        <td id = "district"></td>
                    </tr>
                    </tbody>
                </table>

            <div class="form-actions">
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqExit()">EXIT</button>
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqBack()">BACK</button>
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqUse()">USE NOW</button>
            </div>
</div>
    <!--END DIALOG-->