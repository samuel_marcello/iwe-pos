        <!--START USER DIALOG-->
<div data-role="dialog" id="addUserdialog" class="padding20" data-close-button = "true" data-width = "500" data-height = "550"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>Enter User Details</h1>
        <hr class="thin"/>
        <br/>
        <form role = "form" method = "post" action = "models/logModel.php">
                <div class="input-control text full-size">
                    <label for = "firstName">First Name</label>
                    <input type="text" name = "firstName" id = "firstName" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "lastName">Last Name</label>
                    <input type="text" name = "lastName" id = "lastName" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "email">Email</label>
                    <input type="text" name = "email" id = "email" REQUIRED>
                </div><br/><br/>

                <div class="input-control password full-size" data-role="input">
                    <label for = "password">password</label>
                    <input type="password" name = "password" id = "password" REQUIRED>
                    <button class="button helper-button reveal"><span class="mif-looks"></span></button>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "telephone">Telephone</label>
                    <input type="text" name = "telephone" id = "telephone" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "district">District</label>
                    <input type="text" name = "city" id = "district" REQUIRED>
                </div><br/><br/>

                <div class="input-control select">
                    <label>Role :</label>
                    <select name = "role">
                        <option vlaue = "Admin">Admin</option>
                        <option vlaue = "Accountant">Accountant</option>
                        <option vlaue = "Data entry clerk" selected="selected">Data entry clerk</option>
                    </select>
                </div>

                <div class="input-control select">
                    <label>User Access Level :</label>
                    <select name = "ual">
                        <option vlaue = "0" selected="selected">0</option>
                        <option vlaue = "1">1</option>
                    </select>
                </div>

            <div class="form-actions center">
                <button type="submit" class="button primary" name = "submit" value = "Register">ENTER</button>
            </div>
        </form>
</div>
    <!--END DIALOG-->
