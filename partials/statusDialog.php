<!--START USED VOUCHER DIALOG-->
<div data-role="dialog" id="statusdialog" class="padding20" data-close-button = "true" data-width = "550" data-height = "300"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>OFSP Voucher Used</h1>
        <hr class="thin"/>
        <br/>
                <table class = "table">
                    <theader>
                    <theader>
                    <tbody>
                    <tr>
                        <td>DVM:</td>
                        <td id = "statusDVM"></td>
                        <td>VOUCHERS SCANNED :</td>
                        <td id = "scanned"></td>
                    </tr>
                    <tr>
                        <td>DISTRICT :</td>
                        <td id = "statusdistrict"></td>
                        <td>INVOICES :</td>
                        <td id = "invoices"></td>
                    </tr>
                    </tbody>
                </table>

            <div class="form-actions">
                <button type="submit" class="button primary large-button" name = "submit" value = "sequence" onclick = "seqOk()">OK</button>
            </div>
</div>
    <!--END DIALOG-->