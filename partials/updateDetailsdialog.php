<!--START UPDATE VOUCHER DIALOG-->
<div data-role="dialog" id="updatedialog" class="padding20" data-close-button = "true" data-width = "550" data-height = "650"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>OFSP Voucher Used</h1>
        <hr class="thin"/>
        <br/>

                <div class="input-control text" style = "width: 250px">
                    <label for = "serial">SERIAL</label>
                    <p id = "updateSerial"></p>
                </div>

                <table class = "table bordered">
                    <theader>
                    <tr>
                        <th>DVM ID</th>
                        <th>DVM</th>
                        <th>DISTRICT</th>
                    </tr>
                    <theader>
                    <tbody>
                    <tr>
                        <td id = "updateDVMID"></td>
                        <td id = "updateDVM"></td>
                        <td id = "updateDVMdistrict"></td>
                    </tr>
                    </tbody>
                </table>
                <form role = "form" method = "post" action = "models/farmersModel.php">
                <input type = "hidden" name = "dvm" id = "updateID" value = "">
                <input type = "hidden" name = "vid" id = "updateVID" value = "">  
                              
                <div class="input-control text full-size">
                    <label for = "firstName">First Name</label>
                    <input type="text" name = "firstName" id = "firstName" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "lastName">Last Name</label>
                    <input type="text" name = "lastName" id = "lastName" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label for = "district">District</label>
                    <input type="text" name = "district" id = "district" REQUIRED>
                </div><br/><br/>

                <div class="input-control text full-size">
                    <label class="input-control radio">
                        <input type="radio" name = "sex" value = "male">
                        <span class="check"></span>
                        <span class="caption">Male</span>
                    </label>
                </div><br/><br/> 

                <div class="input-control text full-size">
                    <label class="input-control radio">
                        <input type="radio" name = "sex" value = "female">
                        <span class="check"></span>
                        <span class="caption">Female</span>
                    </label>
                </div><br/><br/> 

            <div class="form-actions center">
                <button type="submit" class="button primary large-button" name = "submit" value = "vupdate">UPDATE</button>
            </div>
        </form>
</div>
    <!--END DIALOG-->