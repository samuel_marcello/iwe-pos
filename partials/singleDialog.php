        <!--START SINGLE SEQUENCE DIALOG-->
<div data-role="dialog" id="singledialog" class="padding20" data-close-button = "true" data-width = "450" data-height = "250"
    data-overlay = "true" data-overlay-color = "op-dark">
        <h1>Enter OFSP Voucher</h1>
        <hr class="thin"/>
        <br/>

                <div class="input-control text" style = "width: 300px">
                    <label for = "serial">BARCODE</label>
                    <input type="text" name = "serial" id = "single">
                </div>
             <button type="submit" class="button primary" name = "submit" value = "validate" onClick = "seqValidate()">ENTER</button>

            <div class="form-actions">
                <button type="clear" class="button primary" name = "exit" value = "exit" onclick = "seqExit()">EXIT</button>
            </div>
</div>
    <!--END DIALOG-->