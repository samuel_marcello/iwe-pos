<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

if(logValidate() == true){  
?>
<html>
<head>
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/jquery.jclock.js"></script>

    <script>
    $(document).ready(function() {$("#single").focus();});
        function seqValidate(){
            var serial = $('#single').val();
            var tr;

            $.post("models/voucherModel.php", { serial: serial, submit: "validate" }, function(data) {
                var vouchers = JSON.parse(data);
                for (var key in vouchers) {
                    if (vouchers.hasOwnProperty(key)) {
                        if (vouchers[key]["status"] == 0) {

                            tr = "<tr>"+
                            "<td>"+vouchers[key]["DVMID"]+"</td>"+
                            "<td>"+vouchers[key]["DVMName"]+"</td>"+
                            "<td></td>"+
                            "<td>"+vouchers[key]["ticketNo"]+"</td>"+
                            "<td>"+vouchers[key]["serialNo"]+
                            "<input type = 'hidden' name = 'barcode[]' value = "+vouchers[key]["serialNo"]+" readonly></td>"+
                            "</tr>";

                            $('table').append(tr);
                        }
                        if (vouchers[key]["status"] == 2) {
                            alert("voucher already used");
                        }
                    }
                }
            });


            $('#single').val("");
            $("#single").focus();

        }
    </script>
</head>
<body>
    <div class = "wrapper">

    <div class = "user">
        <div class = "back">
        <a href = "index.php" class = "log-back">
            <div class = "mn-btn">
                <span class="mif-arrow-left mif-4x bg-transparent fg-darkGreen"></span>
            </div>
            
        </a>
        <div class = "mn-title">
            <span class = "title">BACK</span>
        </div>
        </div>
        <h1 class = "margin30">BATCH VOUCHER PROCESSING</h1>
    </div>
    <hr class="thin"/>
    <br/>

        <div class = "margin20">
        <div class="input-control text" style = "width: 300px">
            <label for = "serial">BARCODE</label>
            <input type="text" name = "serial" id = "single">
        </div>
        <button type="submit" class="button primary" name = "submit" value = "validate" onClick = "seqValidate()">ENTER</button>
        </div>

    <form class = "margin20" method = "post" action = "models/voucherModel.php">
    <table role = "table" class = "table striped cell-hovered bordered">
        <theader>
            <tr>
                <th>DVM ID</th>
                <th>DVM</th>
                <th>DISTRICT</th>
                <th>SEIAL NUMBER</th>
                <th>BARCODE NUMBER</th>
            </tr>
        </theader>
    </table>
    <button type="submit" class="button primary margin10"name = "submit"
    value = "batch">USE NOW</button>
    </form>

</div>
</body>
</html>

<?php
}
else{
    $Vhtqnw2fdigh = "Restricted zone";
    setNotice($Vhtqnw2fdigh);
    header("location:login.php");
}
?>