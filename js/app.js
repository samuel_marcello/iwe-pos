    $(document).ready(function() {

        notify();

        $('#farmers').dataTable({
            "scrollCollapse": true,
            "paging": true
        });

        $('#dvms').dataTable({
            "scrollCollapse": true,
            "paging": true
        });

        $('#vouchers').dataTable({
            "scrollCollapse": true,
            "paging": true
        });

        $('#invoices').dataTable({
            "scrollCollapse": true,
            "paging": true
        });

        $('#receipt').dataTable({
            "scrollCollapse": true,
            "paging": true
        });

        $('.dialog-close-button').click(function() {
            cleaner();
        });

        // SELECT FARMER
        $("#farmers tbody").on('click', 'tr', function() {
            if (!$(this).hasClass('selected')) {
                $('#farmers tr').removeClass('selected');
                $(this).addClass('selected');
            } else {
                $(this).removeClass('selected');
            }
        });

        $('#deleteFarmer').on('click', function(e) {
            var selected = "";
            selected = $("#table tr.selected td:first").html();
            deleteFarmer(selected);
        });

        $('#editFarmer').on('click', function(e) {
            var selected = "";
            selected = $("#table tr.selected td:first").html();
            editFarmer(selected);
        });

        // SELECTING DVM FOR EDITING AND OR DELETING
        $("#dvms tbody").on('click', 'tr', function() {
            if (!$(this).hasClass('selected')) {
                $('#dvms tbody tr').removeClass('selected');
                $(this).addClass('selected');
            } else {
                $(this).removeClass('selected');
            }
        });
        // DVM DELETE INIT
        $('#deleteDVM').on('click', function(e) {
            var selected = "";
            selected = $("#table tr.selected td:first").html();
            deleteDVM(selected);
        });
        // DVM EDIT INIT
        $('#editDVM').on('click', function(e) {
            var selected = "";
            selected = $("#table tr.selected td:first").html();
            editDVM(selected);
        });

        // SELECTING USER FOR DELETING
        $("#users tbody").on('click', 'tr', function() {
            if (!$(this).hasClass('selected')) {
                $('#farmers tr').removeClass('selected');
                $(this).addClass('selected');
            } else {
                $(this).removeClass('selected');
            }
        });
        // USER DELETE INIT
        $('#deleteUser').on('click', function(e) {
            var selected = "";
            selected = $("#users tr.selected td:first").html();
            if (confirm("DELETE USER WITH ID " + selected + " ?") == true) {
                deleteUser(selected);
            }
        });

        // AUTOSEARCH
        $("#repDVM").keyup(function() {
            var name = $('#repDVM').val();
            if (name == "") {
                $("#display").html("");
            } else {
                $.ajax({
                    type: "POST",
                    url: "models/statusReportModel.php",
                    data: "DVMname=" + name + "& submit=search",
                    success: function(html) {
                        $("#display").html(html).show();
                    }
                });
            }
        });

    });

    // NOTIFIER
    function notify() {
        $.post("functions/notice.php", { submit: "notice" }, function(data) {
            var notice = JSON.parse(data);
            for (var key in notice) {
                if (notice.hasOwnProperty(key)) {
                    var msg = notice[key]["text"];

                    if (notice[key]["response"] == "SUCCESS") {
                        $.Notify({
                            caption: 'SUCCESS',
                            content: msg,
                            type: 'success'
                        });
                    }
                    if (notice[key]["response"] == "ERROR") {
                        $.Notify({
                            caption: 'ERROR',
                            content: msg,
                            type: 'warning'
                        });
                    }
                }
            }

        });
    }

    // HTML DOCUMENT CLEANER
    function cleaner() {
        $("#ro-serial").html("");
        $("#DVM").html("");

        $("#usedSerial").html("");
        $("#usedDVM").html("");
        $("#DVMID").html("");

        $("#statusDVM").html("");
        $("#scanned").html("");

        $("#district").html("");
        $("#statusdistrict").html("");
        $("#DVMdistrict").html("");
    }

    // DISPLAYING CURRENT TIME AND DATE
    $(function($) {
        var options3 = {
            format: '%Y-%m-%d %H:%M:%S %P' // 24-hour
        }
        $('#timeOfDay').jclock(options3);
    });

    // CHECKING CHOSEN SEQUENCE OPTION.
    function seqAction() {
        var sequence = $('input[type=radio]:checked').val();
        if (sequence == "single") {
            metroDialog.close('#OFSPdialog');
            metroDialog.open('#singledialog');
            $("#single").focus();
        }
        if (sequence == "batch") {
            metroDialog.close('#OFSPdialog');
            window.location = "batch_sequence.php";
        }
    }

    // CHECKING THE DATABASE FOR THE ENTERED SERIAL NUMBER
    function seqValidate() {
        var serial = $('#single').val();

        $.post("models/voucherModel.php", { serial: serial, submit: "validate" }, function(data) {
            var vouchers = JSON.parse(data);
            for (var key in vouchers) {
                if (vouchers.hasOwnProperty(key)) {
                    if (vouchers[key]["status"] == 0) {
                        $("#ro-serial").append(vouchers[key]["serialNo"]);
                        $("#DVM").append(vouchers[key]["DVMName"]);
                        $("#district").append(vouchers[key]["district"]);

                        metroDialog.close('#singledialog');
                        metroDialog.open('#voucherdialog');
                    }
                    if (vouchers[key]["status"] == 2) {
                        alert("voucher already used");
                        $("#single").val("");
                        metroDialog.close('#singledialog');
                        metroDialog.open('#singledialog');
                        $("#single").focus();
                    }
                }
            }

        });
        $("#single").val("");
    }

    // QUIT THE VOUCHER PROCESS
    function seqExit() {
        window.location = "index.php";
    }

    // QUIT THE VOUCHER PROCESS
    function seqNew() {
        cleaner();
        metroDialog.close('#usedVoucherdialog');
        metroDialog.open('#singledialog');
        $("#single").focus();
    }

    // MOVE BACK TO WINDOW FOR ENTERING SERIALS
    function seqBack() {
        cleaner();
        metroDialog.close('#voucherdialog');
        metroDialog.open('#singledialog');
        $("#single").focus();
    }

    // USE VOUCHER
    function seqUse() {
        var serialNo = $('#ro-serial').html();
        $.post("models/voucherModel.php", { serialNo: serialNo, submit: "use" }, function(data) {
            var voucher = JSON.parse(data);
            for (var key in voucher) {
                if (voucher.hasOwnProperty(key)) {
                    if (voucher[key]["status"] == 1) {
                        $("#usedDVMID").val(voucher[key]["DVMID"]);
                        $("#usedv").val(voucher[key]["ticketID"]);

                        $("#usedSerial").append(voucher[key]["serialNo"]);
                        $("#usedDVM").append(voucher[key]["DVMName"]);
                        $("#DVMID").append(voucher[key]["DVMID"]);
                        $("#DVMdistrict").append(voucher[key]["district"]);

                        metroDialog.close('#voucherdialog');
                        metroDialog.open('#usedVoucherdialog');
                    }
                    if (voucher[key]["status"] == 2) {
                        alert("voucher already used");
                    }

                }
            }

        });

    }

    // STATUS OF THE SEED MULTIPLIER
    function seqStatus() {
        var DVMID = $("#DVMID").html();
        $.post("models/voucherModel.php", { DVMID: DVMID, submit: "status" }, function(data) {
            var status = JSON.parse(data);
            for (var key in status) {
                if (status.hasOwnProperty(key)) {
                    $("#statusDVM").append(status[key]["DVM"]);
                    $("#scanned").append(status[key]["scanned"]);
                    $("#statusdistrict").append(status[key]["district"]);
                    metroDialog.open('#statusdialog');
                }
            }

        });
    }

    // UPDATE OF THE SEED MULTIPLIER
    function seqUpdate() {
        var DVMID = $("#usedDVMID").val();
        var serialNo = $("#usedSerial").html();
        cleaner();
        $.post("models/voucherModel.php", { DVMID: DVMID, submit: "status" }, function(data) {
            var status = JSON.parse(data);
            for (var key in status) {
                if (status.hasOwnProperty(key)) {
                    $("#updateID").val(status[key]["DVMID"]);
                    $("#updateVID").val(serialNo);

                    $("#updateSerial").append(serialNo);
                    $("#updateDVMID").append(status[key]["DVMID"]);
                    $("#updateDVM").append(status[key]["DVM"]);
                    $("#updateDVMdistrict").append(status[key]["district"]);
                    metroDialog.close('#usedVoucherdialog');
                    metroDialog.open('#updatedialog');
                }
            }

        });
    }

    function seqOk() {
        metroDialog.close('#statusdialog');
    }

    function deleteFarmer(farmerID) {
        $.post("models/farmersModel.php", { farmerID: farmerID, submit: "delete" }, function(data) {
            window.location = "farmers.php";
        });
    }

    function editFarmer(farmerID) {
        $.post("models/farmersModel.php", { farmerID: farmerID, submit: "details" }, function(data) {
            var farmer = JSON.parse(data);
            for (var key in farmer) {
                if (farmer.hasOwnProperty(key)) {
                    metroDialog.open('#editFarmerdialog');
                    $("#farmerID").val(farmer[key]["farmerID"]);
                    $("#editfirstName").val(farmer[key]["firstName"]);
                    $("#editlastName").val(farmer[key]["lastName"]);
                    $("#editdistrict").val(farmer[key]["district"]);

                }
            }
        });
    }

    // DELETE DVM
    function deleteDVM(DVMID) {
        $.post("models/DVMModel.php", { DVMID: DVMID, submit: "delete" }, function(data) {
            window.location = "dvm_settings.php";
        });
    }

    // DELETE USER
    function deleteUser(userID) {
        $.post("models/logModel.php", { userID: userID, submit: "delete" }, function(data) {
            notify();
            setTimeout(window.location = "profile.php", 5000);
        });
    }

    // EDIT DVM
    function editDVM(DVMID) {
        $.post("models/DVMModel.php", { DVMID: DVMID, submit: "details" }, function(data) {
            var DVM = JSON.parse(data);
            for (var key in DVM) {
                if (DVM.hasOwnProperty(key)) {
                    metroDialog.open('#editDVMdialog');
                    $("#DVMID").val(DVM[key]["DVMID"]);
                    $("#editDVMName").val(DVM[key]["DVMName"]);
                }
            }
        });
    }

    function pay(invoiceID) {
        $("#pay" + invoiceID).html('<span class="mif-spinner4 mif-ani-spin mif-3x"></span>');
        $.post("models/paymentsModel.php", { invoiceID: invoiceID, submit: "pay" }, function(data) {
            $("#pay" + invoiceID).html('');
            if (data == "success") {
                $.Notify({
                    caption: 'NOTICE',
                    content: 'Payment succeeded.',
                    type: 'success'
                });
                // reload page after 3 seconds
                // window.setTimeout(function(){location.reload()},1000);
            } else {
                $.Notify({
                    caption: 'WARNING',
                    content: 'Payment failed.',
                    type: 'danger'
                });
                // reload page after 3 seconds
                window.setTimeout(function() { location.reload() }, 1000);
            }

        });
    }

    function monthInvoices(month) {
        var payment = $('#doc-payment').val();
        var year = $('#doc-year').val();

        $.post("models/documentsModel.php", { payment: payment, month: month, year: year, submit: "documents" }, function(data) {
            $("#documents").html(docsTable(data));
        });
    }

    function docsTable(data) {
        var invoice = JSON.parse(data);

        table = "<table class = 'table border bordered hovered'>";

        table += "<thead><tr>";
        table += "<th>INVOICE SERIAL</th>";
        table += "<th>DVM NAME</th>";
        table += "<th>INVOICED DATE</th>";
        table += "<th>PAID DATE</th>";
        table += "<th>RECEIPT</th>";
        table += "</rt></thead>";

        table += "<tbody>";

        for (var key in invoice) {
            table += "<tr class = 'docs'>";
            if (invoice.hasOwnProperty(key)) {
                table += "<td>" + invoice[key]["invoiceSerialNo"] + "</td>";
                table += "<td>" + invoice[key]["dvmName"] + "</td>";
                table += "<td>" + invoice[key]["invoiceDate"] + "</td>";
                table += "<td>" + invoice[key]["paidDate"] + "</td>";
                table += "<td><a href = 'receipt.php?invoiceSerialNo=" + invoice[key]["invoiceSerialNo"] + "'>receipt</a></td>";
            }
            table += "</tr>";
        }
        table += "</tbody></table>";

        return table;
    }

    function fill(Value) {
        $('#repDVM').val(Value);
        $('#display').hide();
    }