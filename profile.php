<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

INCLUDE "classes/config.php";
INCLUDE "classes/DBConnection.php";

$Vmije32iljj3 = new DBConnection();
$Vv2oj1ejnszs = $Vmije32iljj3->connection;

if(logValidate() == true){  
$Vfpicy5g1cp0 = $_SESSION['userID'];
?>
<html>
<head>
    <link rel="stylesheet" href="css/jquery.dataTables.css">
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/jquery.dataTables.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/app.js"></script>
    <script src = "js/jquery.jclock.js"></script>
</head>
<body>
    <div class = "wrapper">
<?php
INCLUDE "partials/addUserdialog.php";
INCLUDE "partials/editFarmerDialog.php";
?>
    <div class = "user">
        <div class = "back">
            <a href = "index.php" class = "log-back">
                <div class = "mn-btn">
                    <span class="mif-arrow-left mif-4x bg-transparent fg-darkGreen"></span>
                </div>

            </a>
            <div class = "mn-title">
                <span class = "title">BACK</span>
            </div>
        </div>
        <h1 class = "margin30">PROFILE</h1>
    </div>

    <hr class="thin"/>
    <br/>
    <?php
    $V12iz3ozwqiu = $Vv2oj1ejnszs->query("SELECT * FROM user WHERE userID = $Vfpicy5g1cp0");
    $Vriqs5muj0od = $V12iz3ozwqiu->fetch_assoc();

    ?>
    <div class="flex-grid">
            <div class = "row">
                <div class = "cell colspan6">
                    <div class = "margin10">
                        <span class="mif-user mif-4x bg-transparent fg-darkGreen"></span>
                        <p><strong><?php  echo $Vriqs5muj0od['firstName']." ".$Vriqs5muj0od['lastName']; ?><strong><p>
                    </div>

                    <div class = "margin10">
                        <span class="mif-phone mif-2x bg-transparent fg-darkGreen"> </span>
                        <?php  echo $Vriqs5muj0od['telephone']; ?>
                    </div>

                    <div class = "margin10">
                        <span class="mif-envelop mif-2x bg-transparent fg-darkGreen"> </span>
                        <?php  echo $Vriqs5muj0od['email']; ?>
                    </div>

                    <div class = "margin10">
                        <span class="mif-home mif-2x bg-transparent fg-darkGreen"> </span>
                        <?php  echo $Vriqs5muj0od['city']; ?>
                    </div>
                </div>
                <div class = "cell colspan6">
                    <form methos = "post" action = "models/logModel.php">
                    <input type = "hidden" name = "userID" value = "<?php echo $_SESSION['userID']; ?>">
                        <div class="input-control password full-size" data-role="input">
                            <label for = "password">password</label>
                            <input type="password" name = "password" id = "password" REQUIRED>
                            <button class="button helper-button reveal"><span class="mif-looks"></span></button>
                        </div><br/><br/>

                        <button type = "submit" name = "submit" class="button primary" value = "editPass">EDIT PASSWORD</button>
                    </form>
                </div>
            </div>

            <div class = "row">
                <?php 
                        if($Vriqs5muj0od['role'] == "Admin"){
                    ?>

                    <div class = "margin20">
                        <button class="button primary" name = "submit" onClick = "metroDialog.open('#addUserdialog')">ADD USER</button>
                        <button id = "deleteUser" class="button warning">DELETE USER</button>
                    </div>
            </div>
            <div class = "row">
                <div class = "margin10">
                    <table id = "users" class = "table border bordered hovered">
                        <thead>
                            <tr>
                                <th>USER ID</th>
                                <th>FIRST NAME</th>
                                <th>LAST NAME</th>
                                <th>PHONE</th>
                                <th>EMAIL</th>
                                <th>CITY</th>
                                <th>ROLE</th>
                                <th>LEVEL</th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php 
                    $Vfpicy5g1cp0 = $_SESSION['userID'];
                        $V12iz3ozwqius = $Vv2oj1ejnszs->query("SELECT * FROM user WHERE userID != $Vfpicy5g1cp0");

                        foreach($V12iz3ozwqius as $V12iz3ozwqiu){
                            echo "<tr>
                                    <td>".$V12iz3ozwqiu['userID']."</td>
                                    <td>".$V12iz3ozwqiu['firstName']."</td>
                                    <td>".$V12iz3ozwqiu['lastName']."</td>
                                    <td>".$V12iz3ozwqiu['telephone']."</td>
                                    <td>".$V12iz3ozwqiu['email']."</td>
                                    <td>".$V12iz3ozwqiu['city']."</td>
                                    <td>".$V12iz3ozwqiu['role']."</td>
                                    <td>".$V12iz3ozwqiu['level']."</td>
                                </tr>";
                        }
                    ?>
                        </body>
                    </table>
                    </div>
                        <?php
                        }else{}
                    ?>
            </div>
        </div>

</div>
</body>
</html>

<?php
}
else{
    $Vhtqnw2fdigh = "Restricted zone";
    setNotice($Vhtqnw2fdigh);
    header("location:login.php");
}
?>
