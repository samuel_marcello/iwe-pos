<?php
INCLUDE "functions/sessions.php";
INCLUDE "functions/logValidate.php";
INCLUDE "functions/notifier.php";

if(logValidate() == true){  
    INCLUDE "functions/ACL.php";    
?>
<html>
<head>
    <link rel="stylesheet" href="css/metro.min.css">
    <link rel="stylesheet" href="css/metro-icons.min.css">
    <link rel="stylesheet" href="css/metro-schemes.min.css">
    <link rel="stylesheet" href="css/metro-colors.min.css">
    <link rel="stylesheet" href="css/iwe.css">

    <script src = "js/jquery-3.1.0.min.js"></script>
    <script src = "js/metro.min.js"></script>
    <script src = "js/app.js"></script>
    <script src = "js/jquery.jclock.js"></script>
</head>
<body>
    <div class = "wrapper">

<?php
INCLUDE "partials/sequenceDialog.php";
INCLUDE "partials/singleDialog.php";
INCLUDE "partials/batchDialog.php";
INCLUDE "partials/singleVoucherDialog.php";
INCLUDE "partials/usedVoucherDialog.php";
INCLUDE "partials/statusDialog.php";
INCLUDE "partials/updateDetailsdialog.php";

?>
    <div class = "comapny">
    </div>
    <div class = "banner">
    </div>

    <div class = "user">
        <a href = "profile.php" class = "user-log">
            <div class = "mn-btn">
                <span class="mif-user-check mif-4x bg-transparent fg-darkGreen"></span>
            </div>
            <div class = "mn-title">
                <span class = "title"><?php echo strtoupper($_SESSION['role']); ?></span>
            </div>
        </a>

        <a href = "models/logModel.php?submit=Logout" class = "logout">
            <div class = "mn-btn">
                <span class="mif-exit mif-4x bg-transparent fg-darkGreen"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">LOGOUT</span>
            </div>
        </a>

        <div class = "time">
            <p><strong><span id="timeOfDay"></span></strong></p>
        </div>

    </div>

    <div class = "menu-icons">
        <p>Please click an <strong>icon</strong> to select your prefered action</p>
        <a href = "#" class = "mn-main" onclick="metroDialog.open('#OFSPdialog')">
            <div class = "mn-btn">
                <span class="mif-barcode mif-4x bg-transparent fg-orange mif"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">OFSP VOUCHER</span>
            </div>
        </a>
        <a href = "<?php echo $Vxzaebw02myu; ?>" class = "mn-main">
            <div class = "mn-btn">
                <span class="mif-file-text mif-4x bg-transparent fg-orange"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">STATUS REPORT</span>
            </div>
        </a>

        <a href = "<?php echo $Vtbirov20zv2; ?>" class = "mn-main">
            <div class = "mn-btn">
                <span class="mif-credit-card mif-4x bg-transparent fg-orange"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">PAYMENTS</span>
            </div>
        </a>

        <a href = "<?php echo $V0diybp4mn5m; ?>" class = "mn-main">
            <div class = "mn-btn">
                <span class="mif-folder-open mif-4x bg-transparent fg-orange"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">DOCUMENTS</span>
            </div>
        </a>
        
        <a href = "<?php echo $V1mnnvoliicu; ?>" class = "mn-main">
            <div class = "mn-btn">
                <span class="mif-wifi-connect mif-4x bg-transparent fg-orange"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">CONNECT SERVER</span>
            </div>
        </a>

        <a href = "<?php echo $Vnco4u4f1g0h; ?>" class = "mn-main">
            <div class = "mn-btn">
                <span class="mif-user-plus mif-4x bg-transparent fg-orange"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">FARMERS</span>
            </div>
        </a>

        <a href = "<?php echo $Vqwlaftpiyhd; ?>" class = "mn-main">
            <div class = "mn-btn">
                <span class="mif-cogs mif-4x bg-transparent fg-orange"></span>
            </div>
            <div class = "mn-title">
                <span class = "title">DMV SETTINGS</span>
            </div>
        </a>
    </div>

    <div class = "btm-line">
        <div class = "left-p">
            <p><span class="mif-phone mif-1x bg-transparent fg-orange"> Support: +265 999 600 719</p>
        </div>
        <div class = "right-p">
            <p class = "bg-transparent fg-orange">OFSP|version 1.0|Copyright 2017|IWE Solutions</p>
        </div>
    </div>
    </div>
</body>
</html>

<?php
}
else{
    $Vhtqnw2fdigh = "Restricted zone";
    setNotice($Vhtqnw2fdigh);
    header("location:login.php");
}
?>